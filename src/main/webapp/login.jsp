<%-- 
    Document   : login
    Created on : Apr 28, 2016, 9:27:06 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="login" class="pratica.jsp.LoginBean" scope="session" />
<jsp:useBean id="date" class="java.util.Date" /> 
<jsp:setProperty property="*" name="login"/> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Login</title>
      <link rel='stylesheet' href='css/geral.css' type='text/css'>
   </head>
   <body>
       <div>
           <c:if test="${login.nome!=null and login.login!=null}">
           <c:if test="${!login.nome.equals(login.login)}">
                <div id="erro">
                    Acesso negado
                </div>
            </c:if>
            <c:if test="${login.nome.equals(login.login)}">
                <div id="sucesso">
                    <c:if test="${login.perfil.equalsIgnoreCase('1')}">
                        Cliente, login bem sucedido, para <jsp:getProperty name="login" property="login" /> às <%= date %>
                    </c:if>
                    <c:if test="${login.perfil.equalsIgnoreCase('2')}">
                        Gerente, login bem sucedido, para <jsp:getProperty name="login" property="login" /> às <%= date %>
                    </c:if>
                    <c:if test="${login.perfil.equalsIgnoreCase('3')}">
                       Administrador, login bem sucedido, para <jsp:getProperty name="login" property="login" /> às <%= date %>
                    </c:if>
                </div>
           </c:if>
           </c:if>
       </div> 
     <form method="post" action="/pratica-jsp/login.jsp">
         Código: <input type="text" name="login"/><br/>
         Nome: <input type="text" name="nome"/><br/>
         Perfil: <select name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/>
      </form>
   </body>
</html>