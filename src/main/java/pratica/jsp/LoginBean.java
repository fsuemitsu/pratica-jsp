/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

/**
 *
 * @author user
 */
public class LoginBean {
    
    private String login;
    private String nome;
    private String perfil;

    public LoginBean(){};
    
    public String getLogin() {
        return login;
    }

    public String getNome() {
        return nome;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
}
